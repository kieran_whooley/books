package edu.ait.books;

import edu.ait.books.controllers.BookController;
import edu.ait.books.dto.Book;
import edu.ait.books.repositories.BookRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@DataJpaTest
public class BookControllerTest {

    @InjectMocks
    private BookController bookController;

    @Autowired
    BookRepository bookRepository;

    //test that the DB is empty when started up for testing
    @Test
    public void testDBIsNotEmpty() {
        //Four books are auto-loaded into the in-memory database on start-up
        List<Book> books = bookRepository.findAll();
        assertThat(books).isNotEmpty();
    }

    @Test
    public void createBookTest() {
        Book newBook = bookRepository.save(new Book(5, "Test", "An Author", "2005", "H&H", "Fiction", "Hardback"));
        assertTrue(newBook != null);
    }

    //test that the book is successfully saved to the test database
    @Test
    public void testCreatedBookAttributes() {
        Book book = new Book(5, "Test", "Mise", "2003", "Random House", "Fiction", "Hardback");
        bookRepository.save(book);
        assertThat(book).hasFieldOrPropertyWithValue("title", "Test");
        assertThat(book).hasFieldOrPropertyWithValue("author", "Mise");
        assertThat(book).hasFieldOrPropertyWithValue("publishedYear", "2003");
        assertThat(book).hasFieldOrPropertyWithValue("publisher", "Random House");
        assertThat(book).hasFieldOrPropertyWithValue("genre", "Fiction");
        assertThat(book).hasFieldOrPropertyWithValue("format", "Hardback");

    }

    @Test
    public void testFindAllBooks() {
        //There is already 4 books auto-loaded into the H2 database
        //Add 2 more books to the database
        Book book1 = new Book(6, "Harry Potter", "J.K. Rowling", "1997", "Bloomsbury", "Fantasy", "Paperback");
        bookRepository.save(book1);
        Book book2 = new Book(7, "The Body", "Bill Bryson", "2019", "Doubleday", "Popular Science", "eBook");
        bookRepository.save(book2);

        //get all the books in the database
        List<Book> books = bookRepository.findAll();
        //confirm there are now 6 books in the database
        assertThat(books).hasSize(6);
    }

}
